use std::io::{stdin, stdout, Write};

fn main() {

	let cap = read_input("Enter captcha:");	
    println!("You entered: {} which is {} characters.", cap, cap.len());

	let total_neighbors = add_neighbors(String::clone(&cap));
	let total_rot = add_rot(String::clone(&cap));

	println!("Total neigbors: {}", total_neighbors);
	println!("Total rotated: {}", total_rot);	
}

fn read_input(prompt: &str) -> String {
	println!("{}", prompt);
	let mut input = String::new();
	stdin().read_line(&mut input)
		.expect("Failed to read line");

	println!("{}", input);
	stdout().flush().unwrap();
	String::from(input.trim_right())
}


fn add_neighbors(cap: String) -> i32 {
	let mut sum = 0;
	let len = cap.len();
	
	let first_digit: char = cap.chars().nth(0).unwrap();
	println!("First digit: {}", first_digit);
	for (i, c) in cap.chars().enumerate() {
		let mut next_digit = first_digit;
		if i+1 < len {  // account for null terminator
			println!("{} < {}", i+2, len);
			next_digit = cap.chars().nth(i+1).unwrap();
			println!("Next digit did not wrap: {}", next_digit);
		}
		println!("Looking at char {} at position {}, next_digit = {}", c, i, next_digit);
		
		if c == next_digit {
			sum = sum + c.to_digit(10).unwrap() as i32;
		}
	}
	
	sum
}

fn add_rot(cap: String) -> i32 {
	let mut sum = 0;
	let len = cap.len();
	let half_len = len / 2;
	
	for (i, c) in cap.chars().enumerate() {
		let rot_position = (i + half_len) % len;
		let rot_digit = cap.chars().nth(rot_position).unwrap();
		if c == rot_digit {
			sum = sum + c.to_digit(10).unwrap() as i32;
		}
	}

	sum
}