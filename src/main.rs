use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() {
	let mut spreadsheet: Vec<Vec<u64>> = vec![];

	let stdin = io::stdin();
	for line in stdin.lock().lines() {
		let row = parse_line(&line.unwrap());
		spreadsheet.push(row);
	}

	let mut total_checksum = 0;
	let mut total_divisors = 0;
	for row in spreadsheet.iter() {
		let checksum = calc_line_checksum(row);
		let div = calc_line_divisors(row);
		println!("Row: {:?}\nChecksum: {}\nDivisors: {}", row, checksum, div);
		total_checksum += checksum;
		total_divisors += div;
	}
	
	println!("Total checksum: {}\nTotal div: {}", total_checksum, total_divisors);

}

fn parse_line(line: &str) -> Vec<u64> {

	let mut parsed: Vec<u64> = vec![];
	
	for segment in line.trim_right().trim_left().split(" ") {
		// I'm guessing there's some way to apply parse() to an iterator, but I don't know it.
		parsed.push(segment.parse::<u64>().unwrap());
	}
	
	parsed
}



fn calc_line_checksum(line: &Vec<u64>) -> u64 {
	// I could just use line.max() - line.min() but I'm guessing the input will make that rough
	let mut min: u64 = line[0];
	let mut max: u64 = line[0];
	for x in line.iter() {
		if x < &min {
			min = *x;  // wat
		}
		if x > &max {
			max = *x;  // wat
		}
	}
	max - min
}


fn calc_line_divisors(line: &Vec<u64>) -> u64 {
	let mut div: u64 = 0;
	for (i, x) in line.iter().enumerate() {
		if div != 0 {
			break;
		}
		for y in line.iter().skip(i+1) {
			if x != y && x % y == 0 {
				div = x / y;
				break;
			}
			if x != y && y % x == 0 {
				div = y / x;
				break;
			}
		}
	}
	div
}

